'use strict';
var http = require('http');
var log = require('./log.js');
var sayHello = require('./exportsDemo.js');
var FileStream = require('./FileStream.js')
var port = process.env.PORT || 1337;

http.createServer(function (req, res) {
    if (req.url == "/") {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.write('HomePage\n') //i�erisine html yaz�labilir
    } else {
        res.writeHead(200, { 'Content-Type': 'application/json' }); //200 ba�ar� i�indir, ba�l�k i�in yapar
        res.write(JSON.stringify({name:'Burak',lastname:'Ketmen'}))
    }
    res.end();
}).listen(port);

log.information('Sunucu yayina gecti');
console.log(sayHello());
console.log(FileStream)