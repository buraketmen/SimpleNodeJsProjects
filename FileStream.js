﻿var fs = require('fs');

fs.readFile('example.txt', function (hata, data) {
    if (hata) {
        throw hata;
    }
    console.log(data.toString());
});
fs.readFile('example.txt', "utf8", function (hata, data) {
    if (hata) {
        throw hata;
    }
    console.log(data);
});

fs.writeFile("exampleWrite.txt", "Hasan Burak Ketmen", function (hata) {
    if (hata) {
        throw hata;
    }
    console.log('Yazildi');
});

fs.appendFile("exampleWrite.txt", "Mertcan Ketmen", function (hata) {
    if (hata) {
        throw hata;
    }
    console.log('Yazildi');
});

fs.unlink("exampleWrite.txt", function (hata) {
    if (hata) {
        throw hata;
    }
    console.log('Silindi');
});

