﻿var mongoose = require('mongoose')
var Customer = require('./MongoCustomer.js')

mongoose.connect('mongodbconnectionstring', (error) => {
    if (!error) {
        console.log('Connected to MongoDB')
    }
})

var customer1 = new Customer({
    name: "Burak Ketmen"
    city: "Kocaeli"
})

customer1.save((error)=>{
    if (error) {
        throw error;
    }
    console.log("Customer saved")
})
//her şeyi getirir
Customer.find({}, (error, data) => {
    if (error) {
        throw error;
    }
    console.log(data)
});
//istenen veri getirir
Customer.find({name:'Burak Ketmen'}, (error, data) => {
    if (error) {
        throw error;
    }
    console.log(data)
});
//where().gte(sayi).lte(sayi) iki sayı arasındakileri getirir
//.select('city name') ile istenen dataları getirir
//.sort() ile sıralama
//where ile getirir
Customer.find({}, (error, data) => {
    if (error) {
        throw error;
    }
    console.log(data)
}).where('city').equals('Kocaeli'); //.limit(sayi) ile limit belirlenebilir

//Güncelleme işlemi
Customer.findById('id', (error, data) => {
    if (error) {
        throw error;
    }
    console.log(data)
    data.city = "Sakarya" //sadece arayüzde değiştirir

    //data.remove ile silinebilir
    data.save((error) => {
        if (error) {
            throw error;
        }
        console.log("Customer update") //artık databasede de değişir
    })
});